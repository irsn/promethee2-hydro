# Promethee2-*Hydro*

This is the repository for distribution of Promethee for Hydro'*' studies. It contains Windows, Linux, OSX packages with:

* Code plugins:
  * R
  * Python
  * Excel
  * Telemac
* Algorithms:
  * Brent root finding
  * Gradient descent
  * Efficient Global Optimization (EGO)

---

Downloads:

* [Windows](https://gitlab.com/IRSN/Promethee2-Hydro/-/jobs/artifacts/master/raw/packages/win64b/promethee2-hydro_win64b.zip?job=dist)
* [Linux](https://gitlab.com/IRSN/Promethee2-Hydro/-/jobs/artifacts/master/raw/packages/linux64b/promethee2-hydro_linux64b.zip?job=dist)
* [OSX](https://gitlab.com/IRSN/Promethee2-Hydro/-/jobs/artifacts/master/raw/packages/osx64b/promethee2-hydro_osx64b.zip?job=dist)

![Analytics](https://ga-beacon.appspot.com/UA-109580-11/Promethee2-Hydro)
